#! python3

#june 16, 1995 is the first day of NASA apod

import requests
import json
from datetime import date, timedelta, datetime
from PIL import Image
from io import BytesIO

apod_url       = 'https://api.nasa.gov/planetary/apod'
params         = { 'api_key':'HBsXt4aqFrGPEoD4pQ9pSl92c8kLsy81RBRLk4Ym', 'hd':'true' }
date           = date.today()


# Create a timedelta object to change the date 1 day at a time
dt          = timedelta(days = 1)
waitd       = timedelta(seconds = 5)
imgCount    = 0
nextImgTime = datetime.now()

while imgCount < 50:
    now = datetime.now()
    if now > nextImgTime:
        dateStr        = date.isoformat()
        params['date'] = dateStr

        res = requests.get(apod_url, params=params)
        # parse JSON object into a dict
        res = res.json()

        # skip this day if it's not an image file
        if res['media_type'] != 'image':
            date -= dt
            continue

        img_url = res['url']
        img = requests.get(img_url)
        Image.open(BytesIO(img.content)).show()
        date -= dt
        nextImgTime = now + waitd
